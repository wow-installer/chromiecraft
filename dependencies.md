![picture](chromiecraft-small.png)

# Dependencies

Here are dependencies needed for the distro you use befor running the setup.

&nbsp;&nbsp;&nbsp;&nbsp;

## Debian/Ubuntu

```sudo apt install wine winetricks zenity yad``` 

## Arch/Manjaro Derivatives

```sudo pacman -S wine winetricks zenity yad```

## Fedora

```sudo dnf install wine winetricks zenity yad```

## Void

```sudo xbps-install wine winetricks zenity yad```

&nbsp;&nbsp;&nbsp;&nbsp;

 ### Author
  * Corey Bruce

