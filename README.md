![picture](chromiecraft-small.png)

# ChromieCraft Linux setup Tool

A simple and easy to use graphical setup tool to play Chromiecraft a World of Warcraft private server in Linux

Make sure to install the dependencies on your distro before running the setup.

[Dependencies](dependencies.md)

## How to use:

1. Download the latest release
2. Run the setup script, if you are using Gnome you can right click it and run as program or run it in your terminal
3. Follow along with the installer and select the folder location of the game. Note: if you do change the location of the game you will need to re-run the setup script again as the path of the launch script will be broken so put the game in a place you will keep it like a game drive for example.

It will let you know once it is completed and you will find a shortcut for the game in your system menu under games.


&nbsp;&nbsp;&nbsp;&nbsp;
  
 [Click to get the latest release](https://gitlab.com/wow-installer/chromiecraft/-/releases)

 ### Author
  * Corey Bruce

